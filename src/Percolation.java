public class Percolation
{
    private byte[][] grid;
    private int N;
    private WeightedQuickUnionUF QUF;
    private int counter = 0;

    /**
     * create N-by-N grid, with all sites blocked
     * @param N size
     */
    public Percolation(int N)
    {
        grid = new byte[N][N];
        this.N = N;
        QUF = new WeightedQuickUnionUF(N*N);
        for (int i = 0; i < N - 1; i++)
        {
            QUF.union(i, i + 1);
            QUF.union(N*(N-1) + i, N*(N-1) + i + 1);
        }
    }

    /**
     * Robustness
     * @param i row
     * @param j column
     */
    private void checkIndexes(int i, int j)
    {
        if((i < 1 || i > N) || (j < 1 || j > N))
        {
            throw new IndexOutOfBoundsException();
        }
    }

    public int getCounter()
    {
        return counter;
    }

    /**
     * Open site if it is not already
     * @param i row
     * @param j column
     */
    public void open(int i, int j)
    {
        if(!isOpen(i, j))
        {
            counter++;
            grid[i-1][j-1] = 1;
            if(i > 1 && grid[i-2][j-1] == 1)
                QUF.union(getIndex(i, j), getIndex(i-1, j));
            if(j < N && grid[i-1][j] == 1)
                QUF.union(getIndex(i, j), getIndex(i, j+1));
            if(i < N && grid[i][j-1] == 1)
                QUF.union(getIndex(i, j), getIndex(i+1, j));
            if(j > 1 && grid[i-1][j-2] == 1)
                QUF.union(getIndex(i, j), getIndex(i, j-1));
        }
    }

    /**
     * Give index for QUF
     * @param i row
     * @param j column
     * @return index for QUF
     */
    private int getIndex(int i, int j)
    {
        return N*(i-1) + (j-1);
    }
    /**
     * Is site (row i, column j) open?
     * @param i row
     * @param j column
     * @return is site open
     */
    public boolean isOpen(int i, int j)
    {
        checkIndexes(i, j);
        return grid[i-1][j-1] != 0;
    }

    /**
     * Is site full?
     * @param i row
     * @param j column
     * @return is site full
     */
    public boolean isFull(int i, int j)
    {
        return QUF.connected(0, getIndex(i, j));
    }

    /**
     * Does the system percolate?
     * @return
     */
    public boolean percolates()
    {
        return QUF.connected(0, N*N - 1);
    }
}
