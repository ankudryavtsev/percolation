public class PercolationStats
{
    private int T;
    private double Mean = 0.0, StdDev = 0.0;
    /**
     * Perform T independent computational experiments on an N-by-N grid
     * @param N N-by-N grid
     * @param T independent computational experiments
     */
    public PercolationStats(int N, int T)
    {
        if(N <= 0 || T <= 0)
            throw new IllegalArgumentException();
        this.T = T;
        for (int i = 0; i < T; i++)
        {
            Percolation p = new Percolation(N);
            while(!p.percolates())
            {
                p.open(StdRandom.uniform(1, N+1), StdRandom.uniform(1, N+1));
            }
            update((double)p.getCounter()/(N*N));
        }
    }
    private void update(double newValue)
    {
        Mean += newValue / T;
        StdDev += Math.pow(newValue - Mean, 2) / (T-1) ;
    }

    /**
     * Sample mean of percolation threshold
     * @return
     */
    public double mean()
    {
        return Mean;
    }

    /**
     * Sample standard deviation of percolation threshold
     * @return
     */
    public double stddev()
    {
        return StdDev;
    }

    /**
     * Returns lower bound of the 95% confidence interval
     * @return lower bound of the 95% confidence interval
     */
    public double confidenceLo()
    {
        return mean() - 1.96*Math.sqrt(stddev())/Math.sqrt(T);
    }
    /**
     * Returns upper bound of the 95% confidence interval
     * @return upper bound of the 95% confidence interval
     */
    public double confidenceHi()
    {
        return mean() + 1.96*Math.sqrt(stddev())/Math.sqrt(T);
    }

    /**
     * Test client
     * @param args N = args[0];T = args[1]
     */
    public static void main(String[] args)
    {
        int N = Integer.parseInt(args[0]);
        int T = Integer.parseInt(args[1]);
        PercolationStats stats = new PercolationStats(N, T);
        System.out.println("mean                    = " + stats.mean());
        System.out.println("stddev                  = " + stats.stddev());
        System.out.println("95% confidence interval = " + stats.confidenceLo() + ", " + stats.confidenceHi());
    }
}
